package id.co.telkomsigma.training.controller;

import id.co.telkomsigma.training.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.List;

@Controller("userController")
public class UserController {

    @Autowired
    UserService userService;

    @RequestMapping(value = "/webservice/listUser", method = {RequestMethod.GET})
    public @ResponseBody List listUser() {
        List userList = userService.listUser();
        return userList;
    }

}