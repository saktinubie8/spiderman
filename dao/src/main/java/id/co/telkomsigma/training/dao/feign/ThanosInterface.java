package id.co.telkomsigma.training.dao.feign;


import feign.Headers;
import id.co.telkomsigma.training.dao.pojo.response.AuthTokenInfo;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@FeignClient("thanos")
public interface ThanosInterface {

    @RequestMapping(value = "/thanos/internal/userCheck", method = {RequestMethod.POST})
    String login(@RequestParam String username, @RequestParam String password);

    @RequestMapping(value = "/thanos/webservice/listUser", method = {RequestMethod.GET})
    List listUser();

    @RequestMapping(value = "thanos/oauth/token",method = {RequestMethod.POST})
    AuthTokenInfo loginOauth(@PathVariable("grant_type") String grantType, @PathVariable("username")String username,
                             @PathVariable("password") String password);

}
