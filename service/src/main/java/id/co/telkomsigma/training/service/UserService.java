package id.co.telkomsigma.training.service;

import id.co.telkomsigma.training.dao.feign.ThanosInterface;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service("userService")
public class UserService {

    @Autowired
    private ThanosInterface thanosInterface;

    public List listUser() {
        return thanosInterface.listUser();
    }

}
