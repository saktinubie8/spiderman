package id.co.telkomsigma.training.service;

import id.co.telkomsigma.training.dao.feign.ThanosInterface;
import id.co.telkomsigma.training.dao.pojo.response.AuthTokenInfo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service("loginService")
public class LoginService {

    @Autowired
    private ThanosInterface thanosInterface;

    public String userCheck(String username, String password) {
        return thanosInterface.login(username,password);
    }

    public AuthTokenInfo login(String username, String password){
        return thanosInterface.loginOauth("password", username, password);
    }

}
